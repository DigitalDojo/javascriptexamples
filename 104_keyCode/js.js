// all of the JavaScript is here

// this listens for the user to lift up on a key then runs the keyCheck() function
document.onkeyup = keyCheck;

function keyCheck() {
	
	// here we set a variable "output" to the document element "output"
	// this makes is more convenient to reference later on
	// it basically saves us from typing out document.getElementById('output') every time
	var output = document.getElementById('output');
	
	// this assigns the keyCode for whatever key the user
	// pressed to a variable named keyID 
	var keyID = event.keyCode;
	
	// this puts the keyCode into the console for us to check
	console.log(keyID);
	
	// here we run some conditional tests on different
	// possible keys the user might have pressed. If they match
	// then we give them some specific output just to check that our 
	// program is working properly.  If the user presses a key that isn't
	// being tested then it will just tell them the keyCode for that key.
	// This is a useful resource for future projects as it will tell you
	// the keyCode for any key.
	if (keyID == 16) {
		output.innerHTML = "Shift: " + keyID;
	} else if (keyID == 17) {
		output.innerHTML = "Ctrl: " + keyID;
	} else if (keyID == 18) {
		output.innerHTML = "Alt: " + keyID;
	} else if (keyID == 27) {
		output.innerHTML = "Esc: " + keyID;
	} else if (keyID == 37) {
		output.innerHTML = "Left Arrow: " + keyID;
	} else if (keyID == 38) {
		output.innerHTML = "Up Arrow: " + keyID;
	} else if (keyID == 39) {
		output.innerHTML = "Right Arrow: " + keyID;
	} else if (keyID == 32) {
		output.innerHTML = "Space Bar: " + keyID;
	} else if (keyID == 40) {
		output.innerHTML = "Down Arrow: " + keyID;
	} else {
		// if the key isn't one of the above then just tell them the keyCode 
		output.innerHTML = "event.keyCode: " + keyID;
	}
} // END OF function keyCheck() {
