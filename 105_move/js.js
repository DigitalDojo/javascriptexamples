function move(event) {
  // declare an empty string to hold our message
  let message = '';

  // Assigning the page elements to variables makes them
  // more convenient to reference later on
  let output = document.getElementById('output');
  let ball = document.getElementById('ball');

  // This assigns the keyCode for whatever key the user
  // pressed to a variable named keyID
  let keyID = event.keyCode;

  // Calculate and store some of the ball coordinates:
  let ballLeftPos = ball.offsetLeft;
  let ballRightPos = ballLeftPos + ball.offsetWidth;
  let ballTopPos = ball.offsetTop;
  let ballBottomPos = ballTopPos + ball.offsetHeight;

  // This a multiplier. the box will move by 20 pixels on every step
  let speed = 20;

  // now we test for the key that the user has
  // pressed and move the ball around the page
  if (keyID == 39) {
    // change the output div to display the keyCode
    message = 'Right Arrow: ' + keyID;
    // Change the position by altering the CSS style:
    ball.style.left = ballLeftPos + speed + 'px';
  } else if (keyID == 37) {
    message = 'Left Arrow: ' + keyID;
    // we multiply the position by -1 to make it a negative value which will make it move to the left
    ball.style.left = ballLeftPos + speed * -1 + 'px';
  } else if (keyID == 40) {
    message = 'Down Arrow: ' + keyID;
    ball.style.top = ballTopPos + speed + 'px';
  } else if (keyID == 38) {
    message = 'Up Arrow: ' + keyID;
    // we multiply the position by -1 to make it a negative value which will make it move up the page
    ball.style.top = ballTopPos + speed * -1 + 'px';
  } else {
    // just output the keyCode
    message = 'KeyCode: ' + keyID;
  }

  output.innerHTML = message;
  console.log(message);
}

document.addEventListener('keydown', function (event) {
  move(event);
});
