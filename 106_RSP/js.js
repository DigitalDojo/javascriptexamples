// our event listeners go inside the document ready function
// so that the code inside is available once the page has loaded
$(document).ready(function() {

  $('#container_button').hide();
  $('#round-container').hide();
  $('#announceModal').hide();

  
  var resourcePath = 'resources/';
  var imagePath = 'resources/images/';
  var audioPath = 'resources/audio/';

  var preloadArray = ['bg-1.jpg', 'bg-2.jpg', 'bg-3.jpg', 'bg-4.jpg', 'bg-5.jpg', 'bg-6.jpg', 'bg-7.jpg',
    'leftPaper.png', 'leftRock.png', 'leftScissors.png', 'rightPaper.png', 'rightRock.png', 'rightScissors.png',
    'cheers.mp3', 'death.mp3', 'fight.mp3', 'intro.mp3', 'laugh.mp3', 'round1.mp3', 'round2.mp3', 'round3.mp3', 'select.mp3',
    'youlose.mp3', 'youwin.mp3'
  ];

  var imageObject = {};
  var audioObject = {};

  var userWins = 0;
  var aiWins = 0;
  var maxgames = 3;
  var gamesPlayed = 0;

  preload(preloadArray).done(function() {

    $('#container_button').fadeIn();
    //console.log(imageObject);

  });



  $('#container_button').mouseup(function() {

    $('#splash-container').fadeOut();

    audioObject.intro[0].play();

    playRound(1);

  });





  $('.userChoice').mouseup(function() {

    audioObject.select[0].play();
    
    var aiChoice = Math.floor(Math.random() * 3) + 1;
    //console.log(aiChoice);

    var userChoice = this.id;

    cycleAiHand('stop', aiChoice);

    if (userChoice == 1) {
      $('#userHand-container').html(imageObject.leftRock.show());
    } else if (userChoice == 2) {
      $('#userHand-container').html(imageObject.leftScissors.show());
    } else if (userChoice == 3) {
      $('#userHand-container').html(imageObject.leftPaper.show());
    }
    
    testWinLoss(userChoice, aiChoice);
  });



  
  
  
  
  function testWinLoss(userChoice, aiChoice) {
    
    var userWin = false;
    
    if (userChoice == 1 && aiChoice == 2) {
      userWin = true;
    } else if (userChoice == 2 && aiChoice == 3) {
      userWin = true;
    } else if (userChoice == 3 && aiChoice == 1) {
      userWin = true;
    }
    
   
    
    if (userChoice == aiChoice) {
      
      // draw
      announceText('DRAW');
      
    } else if (userWin === true) {
      
      // user wins, ai loses
      userWins++;
      aiWins--;
       
      announceText('YOU WIN');
       
      setTimeout(function() {
        audioObject.youwin[0].play();
      }, 400);
      
      setTimeout(function() {
        audioObject.cheers[0].play();
      }, 1200);
      
    } else {
      
      // ai wins, user loses
      userWins--;
      aiWins++;
      
      announceText('YOU LOSE');
      
      setTimeout(function() {
        audioObject.youlose[0].play();
      }, 400);
            
      setTimeout(function() {
        audioObject.death[0].play();
      }, 1200);
      
    }
    
    gamesPlayed++;
    
    if (gamesPlayed >= maxgames) {
    
      // end
    } else {
      
     // present choices again for another round 
    }
    
    
    
  }



  function playRound(roundNumber) {

    $('#round-container').fadeIn();

    randomBgImage();

    setTimeout(function() {
      roundAnnounce(roundNumber);
    }, 3200);

    cycleAiHand('start', false);

  }






  function roundAnnounce(roundNumber) {

    
    
    var index = 'round' + roundNumber;
    
    announceText('ROUND '+roundNumber);
    
    audioObject[index][0].play();
    setTimeout(function() {
      audioObject.fight[0].play();
    announceText('FIGHT!');
    }, 1500);


  }

  
  
  function announceText(textToAnnounce) {
    
    //console.log(textToAnnounce);
    
    $('#announceModal').fadeIn(100);
    $('#announceText').text(textToAnnounce);
    $('#announceModal').fadeOut(1300);

  }
  






  function cycleAiHand(startOrStop, stopOn) {

    if (startOrStop == 'start') {

      $('#aiHand-container').html(imageObject.rightRock);
      $('#aiHand-container').append(imageObject.rightScissors);
      $('#aiHand-container').append(imageObject.rightPaper);

      $('#aiHand-container').cycle({
        fx: 'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
        speed: 1,
        timeout: 70
      });

    } else {

      $('#aiHand-container').cycle('stop');
      $("#aiHand-container").removeAttr('style');

      if (stopOn == 1) {
        $('#aiHand-container').html(imageObject.rightRock.removeAttr('style'));
      } else if (stopOn == 2) {
        $('#aiHand-container').html(imageObject.rightScissors.removeAttr('style'));
      } else if (stopOn == 3) {
        $('#aiHand-container').html(imageObject.rightPaper.removeAttr('style'));
      }
    }

  } // END OF function cycleAiHand(startOrStop, stopOn) {








  function randomBgImage() {

    var random = Math.floor(Math.random() * 7) + 1;

    var index = 'bg-' + random;
    //console.log('index: ' + index);

    var imgName = imageObject[index].attr('src');

    $('html').css('transition', 'background 1s linear');
    $('html').css('-webkit-transition', 'background 1s linear');
    $('html').css('-moz-transition', 'background 1s linear');
    $('html').css('-o-transition', 'background 1s linear');
    $('html').css('-ms-transition', 'background 1s linear');

    $('html').css('background', "url(" + imgName + ")");
    $('html').css('background-size', 'cover');
    $('html').css('background-position', 'center center');
    $('html').css('background-repeat', 'no-repeat');
    $('html').css('background-attachment', 'fixed');
    $('html').css('background-color', '#000');

  }






  function preload(preloadArray) {

    var dfrd1 = $.Deferred();

    setTimeout(function() {

      $(preloadArray).each(function() {

        if (this.search(".jpg") >= 0 || this.search(".png") >= 0) {

          var ipath = imagePath + this;

          var iname = this.slice(0, -4);

          imageObject[iname] = $('<img />').attr('src', ipath).appendTo('body').hide();

        } else if (this.search(".mp3") >= 0) {

          var apath = audioPath + this;

          var aname = this.slice(0, -4);

          audioObject[aname] = $("<audio>", {
            src: apath,
            preload: "auto"
          }).appendTo("body");

        }
      });
      dfrd1.resolve();
    }, 1000);

    return $.when(dfrd1).done(function() {
      //console.log(' tasks in function are done');
      // asyncs tasks are done
    }).promise();

  } // END OF function preload(preloadArray) {



}); // END OF $(document).ready(function() {