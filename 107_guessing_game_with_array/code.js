// guess a number between 1 and this
let number_limit = 20;

// generate a random number between 1 and 'number_limit'
let randomNum = Math.ceil(Math.random() * number_limit);

// this is an empty array that will hold the user's guesses
let userAnswers = [];

function checkUserInput() {
  // get user input
  userNum = document.getElementById('input').value;

  // this pushes the user's guess onto the array that holds their guesses
  userAnswers.push(userNum);

  // puts all of the numbers into the console so that we can "cheat" while testing
  console.log('randomNum: ' + randomNum);
  console.log('userNum: ' + userNum);
  console.log('userAnswers: ' + userAnswers);

  // compare user input to randomly generated number
  if (userNum == randomNum) {
    let finalText = '<br/>You won! The correct number is: ' + randomNum;

    // here is where you iterate through the array of user's answers, show them their history and count the number of attempts

    finalText += '<br/>Your guessing history is: ';

    for (guess of userAnswers) {
      finalText += guess + ' ';
    }

    finalText += '<br/>It took you ' + userAnswers.length + ' attempts.';

    // outputs text to the user verifying that they've won
    document.getElementById('output').innerHTML = finalText;
  } else if (userNum > number_limit || userNum < 1) {
    // if the user guesses a number outside of our range
    document.getElementById('output').innerHTML =
      'Unfortunately ' +
      userNum +
      ' is not in the range of numbers (1-' +
      number_limit +
      '). Try again.';
  } else if (userNum > randomNum) {
    // if the user guesses a number that's too high. We let them know.
    document.getElementById('output').innerHTML =
      'Unfortunately ' + userNum + ' is too high. Try again';
  } else if (userNum < randomNum) {
    // if the user guesses a number that's too low. We let them know.
    document.getElementById('output').innerHTML =
      'Unfortunately ' + userNum + ' is too low. Try again';
  }
}

window.onload = function () {
  document.getElementById('button').addEventListener('click', checkUserInput);
  document.getElementById('instructions').innerText =
    'Guess a number between 1 and ' + number_limit;
};
