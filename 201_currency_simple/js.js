   

// here is a function that we've named convertCurrency
// it just sits here and waits for something to use it.
// In our case we have an onclick event listener attached
// to a button on the user interface
function convertCurrency() {
  
  // this retrieves whatever value the user has typed into our input with the id  "amount"
  var USamount = document.getElementById("amount").value;

  // this is a hard-coded value of the inverted exchange rate
  // in an actually useful app, this would be updated regularly via an API
  var US2NZexchangeRate = 1.44116;

  // here we make a new variable called "converted" which multiplies the user's 
  // inputted amount by the exchange rate and then rounds it to two decimal places
  var converted = (USamount * US2NZexchangeRate).toFixed(2);

  // outputText is a concatenated string variable that contains the values
  // that we calculated above. Even though it is partly formed of numeric variables
  // it is treated like a sting because it contains textual data
  var outputText = 'US$' + USamount + ' = NZ$' + converted;

  // this puts the string we just made above into the empty 
  // div with the id "output" in our HTML doc
  document.getElementById("output").innerHTML = outputText;
}