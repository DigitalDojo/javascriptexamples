$(document).ready(function() {

  // barcode lookups
  // https://upcdatabase.org/apiusage
  // api token:  C2844B8A79C365ACF64C4DC3CA4B9357
  

  $('#barcodeInput').focus();
  //   $('#barcodeInput').val('9781408855713');


  $('#output').hide();
  $('#error').hide();


  // listens for when the input field loses focus
  $("#barcodeInput").focusout(function() {



    // grab the value from input field
    let fullBarCode = $(this).val();

    $(this).css("background-color", "#333");


    // if input has more than 2 digits
    let x = fullBarCode
    let digLen = x.toString().length;

    if (digLen < 3) {

      $('#error').fadeIn();
      $('#error').html('<span class="check" style="font-size:2em">You need at least 3 digits for this to work</span>');

    } else {

      $('#error').hide();
      $('#output').fadeIn();

      // we need to isolate the 12 digit barcode from the checksum
      // empty array of digits
      let digits = [],

        // convert barcode to a string
        sNumber = fullBarCode.toString();

      for (var i = 0, len = sNumber.length; i < len; i += 1) {

        // add each digit to array
        digits.push(+sNumber.charAt(i));
      }


      // grab the checksum off the end of array
      let checksum = parseInt(digits.pop());

      // output checksum
      $('#checksum').html(checksum);

      // mash digit array together and convert it back to a number
      let barcode = parseInt(digits.join(""));

      // output 12 digit  barcode
      $('#barCode').html(barcode);

      // run function to calculateCheckSum
      calculateCheckSum(digits, checksum);

      // do api lookup of product details
      getProductDetails(fullBarCode)
    }

  }); // $("#barcodeInput").focusout(function() {





  // this function runs through the barcode and calculates the check sum 
  function calculateCheckSum(digits, checksum) {

    // start off with 0
    var total = 0;
    var calculatedCheckSum;

    // loop through digits and multiply every other digit by 3
    for (i = 0; i < digits.length; i++) {

      var thisNum = digits[i];

      // output this digit into table cell
      $('#' + i).html(thisNum);

      // check for even numbers
      if (i % 2 === 0) {

        total += thisNum;

        // output this calculation into table cell
        if (i ==0) {
          $('#' + i + 'calc').html(thisNum);
        } else {
          $('#' + i + 'calc').html('+ '+thisNum);
        }

      } else {

        // every other number gets multiplied by 3 and added to total
        total += thisNum * 3;
        // output this calculation into table cell
        $('#' + i + 'calc').html('+ '+thisNum * 3);

      }
    }


    //     console.log(total);


    // here is where we calculate the check sum from total

    // holds the nearest higher number divisible evenly by 10
    var nearestDiv;

    // keep trying numbers until we get one perfectly divisble by 10
    for (y = 0; y < 9; y++) {

      var testNum = total + y;

      // check if divisible by 10
      if (testNum % 10 === 0) {

        //         console.log(y);
        calculatedCheckSum = y;

        // found it
        nearestDiv = testNum;
      }
    }

    // make lots of output to show calculations

    var resultHTML = '<td colspan="4">Total = ' + total + '</td>';

    resultHTML += '<td colspan="6">Next highest # divisible by 10 = ' + nearestDiv + '</td>';

    resultHTML += '<td colspan="2" align="right">' + nearestDiv + ' - ' + total + ' = </td>';

    resultHTML += '<td class="check">' + calculatedCheckSum + '</td>';

    $('#result').html(resultHTML);




    // present pass if the calculated checksum equals the scanned checksum
    if (calculatedCheckSum == checksum) {

      $('#passOrFail').html('PASS');
      $('#passOrFail').css("background-color", "#2B9F5A");
      $('.check').css("background-color", "#2B9F5A");
      $('.check').css("color", "#490964");
      $('#passOrFail').css("color", "#490964");
    } else {

      $('#passOrFail').html('FAIL');
      $('#passOrFail').css("background-color", "#BE311B");
      $('.check').css("background-color", "#BE311B");
      $('.check').css("color", "#ffcc33");
      $('#passOrFail').css("color", "#ffcc33");
    }
  }

  function getProductDetails(fullBarCode) {
 
    // 9781408855713

    let apiKey = 'C2844B8A79C365ACF64C4DC3CA4B9357';

    let proxURL = 'https://cors-anywhere.herokuapp.com/';
    let apiURL = 'https://api.upcdatabase.org/product/'+fullBarCode+'?apikey='+apiKey;


    $.getJSON(proxURL+apiURL, function(result){
      $.each(result, function(i, field){
        // $("div").append(field + " ");
        console.log(result);
        $('#productDetails').html = JSON.stringify(result); 
        if (result.success) {
          $('#productDetails').html = JSON.stringify(result); 
        } else {
          $('#productDetails').html = '<h1>Product not in database.</h1>';
        }
      });
    });

    // fetch(proxURL+apiURL)
    // // fetch(apiURL)
    // .then((resp) => resp.json())
    // .then(function(data) {
    //   // Here you get the data to modify as you please
    //   console.log(data.items[0]);
    //   console.log(JSON.stringify(data.items[0]));
    //   $('#productDetails').html = JSON.stringify(data.items[0]);
    // })
    // .catch(e => {
    //   console.log(e);
    //   return e;
    // }); 


  }
});