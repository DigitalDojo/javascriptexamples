
var scrollInterval = 12000; // in milliseconds
var fadeTime = 1400; // in milliseconds
var noticeCursor = 0; // keeps track of which announcement needs to be at the top
var noticeData = []; 
var initTime = new Date();

if (location.hostname.match('nayland')){

  var JSONURI = '../get_json.php';
  var parseIt = false;
} else {
  var JSONURI = 'https://notices.digitaldojo.nz/get_json.php';
  var parseIt = true;
}



$(document).ready(function () {

  initTime = moment(initTime);
  $('body').css('cursor', 'none');

  clock();

  getAllJSON();

});

 
// this loops over and over updating the clock
// uses moment.js 
function clock() {
  var now = moment();
  var today = moment().format('dddd D MMMM YYYY');
  var time = moment().format('LT');

  var reloadTime = moment(initTime).add(1, 'hour');

  if (now > reloadTime) {
    console.log("reloading ...");
    window.location.reload();
  }

  $('#title-2').html(today);
  $('#title-3').html(time);
  setTimeout(function () { clock() }, 500);
}



function getAllJSON() {

  var msBeforeAjaxCall = new Date().getTime();

  $.ajax(JSONURI, {
    "type": "GET",
    "timeout": 5000
  })
    .done(function (data, textStatus, jqXHR) {
      // Process data, as received in data parameter

      // Send warning log message if response took longer than 2 seconds
      var msAfterAjaxCall = new Date().getTime();
      var timeTakenInMs = msAfterAjaxCall - msBeforeAjaxCall;

      var msg = "Successfully fetched notice JSON data from: " + JSONURI;
      if (timeTakenInMs > 2000) {

        msg = "AJAX response to " + JSONURI + " took a long time: " + timeTakenInMs / 1000 + " seconds.";
      }
      console.log(msg);
      parseJSON(data);
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      // Request failed. Show error message to user. 
      // errorThrown has error message, or "timeout" in case of timeout.

      msg = 'Error fetching JSON data. Try reloading the page with SHIFT+F5';
      msg += '<br><br>\n errorThrown: ' + errorThrown;
      msg += '<br><br>\n status code: '+jqXHR.status;
      msg += '<br><br>\n responseText: '+jqXHR.responseText;
 
      console.log(msg);
      $('#mainAnnouncementDiv').prepend(msg);
      $('#mainAnnouncementDiv').show(fadeTime);


    })
    .always(function (jqXHR, textStatus, errorThrown) {
      // this always runs
      console.log("AJAX call finished.");
    });



}





function parseJSON(rawData) {

  // console.log(rawData);

  if (parseIt === true) {
    var rawJSON = JSON.parse(rawData);
  } else {
    var rawJSON = rawData;
  }
  

  // this might be useful
  // its sort of a strange kind of timeStamp
  // ex:  20190529184116
  var dtime = rawJSON.SMSDirectoryData.datetime;
  console.log('dtime: ' + dtime);

  create_notice_data(rawJSON);
}

function create_notice_data(rawJSON) {

  // console.log(rawJSON);

  now = moment().startOf('day');

  numRawEntries = rawJSON.SMSDirectoryData.notices.count;

  console.log("numRawEntries: " + numRawEntries);

  noticeJSON = rawJSON.SMSDirectoryData.notices.data;

  $(noticeJSON).each(function (i, notice) {

    // "uuid": "G8MSHEEEW218CJ3RZIM0ZXRO",
    // "DateStart": "20190528",
    // "DateFinish": "20190529",
    // "PublishWeb": true,
    // "Level": "3. Cultural",
    // "Subject": "Dance Company",
    // "Body": "is on Wednesday afternoons in the PAC. Come and join us to learn the dances for Vast Dance Festival, and to have some dancing fun!",
    // "Teacher": "DHD",
    // "MeetingDate": "00000000",
    // "MeetingTime": "000000",
    // "MeetingPlace": null

    var dateStart = moment(notice.DateStart, "YYYYMMDD");
    var dateFinish = moment(notice.DateFinish, "YYYYMMDD");

    // console.log(" - - - - - - - - - - - - - - - ");
    // console.log("pub: "+notice.PublishWeb);
    // console.log("sub: "+notice.Subject);
    // console.log("dateStart: "+dateStart.format("DD/MM"));
    // console.log("now: "+now); 
    // console.log("dateFinish: "+dateFinish);

    if (notice.PublishWeb === true && now <= dateFinish && dateStart <= now) {

      var id = notice.uuid;

      var tempArray = [];

      tempArray["dateStart"] = dateStart;
      tempArray["dateFinish"] = dateStart;
      tempArray["category"] = notice.Level;
      tempArray["subject"] = notice.Subject;
      tempArray["details"] = notice.Body;
      tempArray["subject"] = notice.Subject;
      tempArray["teacher"] = notice.Teacher;
      tempArray["meetDate"] = notice.MeetingDate;
      tempArray["meetTime"] = notice.MeetingTime;
      tempArray["meetPlace"] = notice.MeetingPlace;

      noticeData.push(tempArray);
      console.log("entered: " + tempArray["subject"]);

    }

  });


  console.log("# of notices: " + noticeData.length);

  // console.log(noticeJSON);
  initNotices(noticeData);
}



function initNotices(noticeData) {

  var revNotices = noticeData;

  revNotices.reverse();

  $(revNotices).each(function (i, notice) {


    var thisElement = createNoticeHTML(notice);
    $('#mainAnnouncementDiv').hide();

    $('#mainAnnouncementDiv').append(thisElement);
    // $('#mainAnnouncementDiv').hide();
    $('#mainAnnouncementDiv').children().show("slow");


    // console.log("i: " + i + " | sub: " + notice.subject); 
  
    // var numChild = $("#mainAnnouncementDiv").children().length;
    // console.log("numChild:" + numChild);

  });

  // when notices are populated, create the (invisible) scroll bar
  create_scroll_bar();
  $('#mainAnnouncementDiv').show(fadeTime);
 
}




function start_scroll_down(scrollInterval) {

  var scroll = setInterval(function () {

    newElement = getNewElement();

    $(".announcements .mCSB_container").append($(newElement).show());
 
    // get rid of top element in presentation
    // to keep it from getting huge
    var topKid = $('.mCSB_container').children().first();
    
    topKid.hide(fadeTime, "swing", function () { 
      $(this).remove();
    }); 

    // var numChild = $(".mCSB_container").children().length;
    // console.log("numChild:" + numChild);

    $(".announcements").mCustomScrollbar("update");

  }, scrollInterval);
}




function getNewElement() {

  var notice = noticeData[noticeCursor];

  var newElement = createNoticeHTML(notice);
  // console.log("noticeCursor: "+noticeCursor);
  // console.log("length: "+noticeData.length);

  if (noticeCursor + 1 < noticeData.length) {
    noticeCursor++;
  } else {
    noticeCursor = 0;
  }

  return newElement;
}





function createNoticeHTML(notice) {
 
  var noticeHTML = '<div class="entry" style="display: none;"><table><tbody>';

    //  ----------- row -----------
    noticeHTML += '<tr>';
      noticeHTML += '<td class="subject" colspan="2">' + notice.subject + '</td>';
      noticeHTML += '<td class="teacher">' + notice.teacher + '</td>';
    noticeHTML += '</tr>';
 
    //  ----------- row -----------
    noticeHTML += '<tr class="placeTime">'; 
 
    noticeHTML += '<td>';

      if (notice.meetPlace !== null) {
        noticeHTML += 'Place: ' + notice.meetPlace;
      }

    noticeHTML += '</td><td>';

      if (notice.meetDate !== '00000000') {
        noticeHTML += 'Date: ' + moment(notice.meetDate,'YYYYMMDD').format('dddd Do MMMM');
      }

    noticeHTML += '</td><td>';

      if (notice.meetTime !== '000000') { 

        noticeHTML += 'Time: ' + moment(notice.meetTime,'HHmm00').format('LT');
      }

    noticeHTML += '</td></tr>';
   
    //  ----------- row -----------
    noticeHTML += '<tr><td class="details" colspan="3">' + notice.details + '</tr>';

  noticeHTML += '</tbody></table></div>';  
    
    // didn't use:
    // notice.category

  return noticeHTML;
}



function create_scroll_bar() {

  $(".announcements").mCustomScrollbar({
    disable: true,
    theme: "dark-thin",
    axis: "y",
    scrollbarPosition: "inside",
    autoHideScrollbar: true,
    advanced: {
      updateOnContentResize: true
    },
    mouseWheel: {
      preventDefault: true
    },
    callbacks: {
      onInit: function () {
        start_scroll_down(scrollInterval);
        $('.announcements').mCustomScrollbar('scrollTo', 'top');

      },
      onSelectorChange: function () {
        console.log($(".announcements").scrollbarPosition());
        console.log('hi');
      },
      onUpdate: function () {
        $('.announcements').mCustomScrollbar('scrollTo', 'top');
      }
    }
  }); 
}
