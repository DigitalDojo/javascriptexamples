$(document).ready(function () {

	let songState = false;
	let gongState = true;

	$('#minutes').focus();

	var clock = $('.clock').FlipClock(0, {
		countdown: true,
		autoStart: false,
		clockFace: 'MinuteCounter',
	});


	var gong = $("<audio>", {
		src: "media/gong.mp3",
		preload: "auto"
	}).appendTo("body");

	var eagle = $("<audio>", {
		src: "media/eagle.mp3",
		preload: "auto"
	}).appendTo("body");


	$('#go').click(function () {

		var minutes = $('#minutes').val();
		var seconds = minutes * 60;
		makeClock(seconds);
	});

	$('#playStop').click(function () {
		
		if (songState === false) {
			$('#playStop').text("music_note");
			songState = true;
		} else {
			$('#playStop').text("music_off");
			songState = false;
		}
	});


	$('#gong').click(function () {
		
		if (gongState === false) {
			$('#gong').text("notifications_active");
			gongState = true;
		} else {
			$('#gong').text("notifications_off");
			gongState = false;
		}
	});





	$('#minutes').keypress(function (e) {
		if (e.which == 13) {
			var minutes = $('#minutes').val();
			var seconds = minutes * 60;
			makeClock(seconds);
		}


	});






	function makeClock(seconds, clock) {

		gong[0].play();
		clock = $('.clock').FlipClock(seconds, {

			autoStart: false,
			countdown: true,
			clockFace: 'MinuteCounter',
			callbacks: {
				stop: function () {
					if (gongState === true) {
						gong[0].play();
					}
				},

				interval: function () {
					var time = clock.getTime().time;
					// 					console.log(time);
					if (time == 300 && songState === true) {
						//Change element
						eagle[0].volume = 0.5;
						eagle[0].play();
					}
				}
			}
		});
		clock.start();
	}




});