<!doctype html>
<html>

<head>
  <title>Javascript Examples</title>
    <link href='https://fonts.googleapis.com/css?family=Nixie+One' rel='stylesheet' type='text/css'>
    <link href='../resources/style.css' rel='stylesheet' type='text/css'>


</head>

<body>
  <div id="container">
    <div id="main">
       
      <h2 id="title">Javascript Examples</h2>

      <?php 
			
      date_default_timezone_set('Pacific/Auckland');
      $date=date('l jS \of F Y h:i:s A');
      echo '<div id="date">Today is '.$date.'</div>';
			echo '<a href="/phpmyadmin/" target="_blank">phpmyadmin</a><br><br>';
      $courseDir='.';
      $filesAndFolders=scandir($courseDir);
			
			$exclude = array('.','..','.git','.htaccess','index.php','resources');
			
			
      //     print_r($filesAndFolders);

      foreach ($filesAndFolders as $listing) {
        if (!in_array($listing, $exclude)) {
          echo '<div class="folderBlock"><a href="'.$listing.'">
					<img src="../resources/metal_folder.png" /><br>'.$listing.'</a></div>';
        }
      }

			?> 
   </div>
  </div>
</body>

</html>